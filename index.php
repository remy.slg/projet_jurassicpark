<?php
require "vendor/autoload.php";

$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
$twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
});

//-------------------------

Flight::route('/', function(){
    Flight::view()->display('liste_dinos.twig');
});

Flight::route('/dinosaur/brachiosaurus/', function(){
    Flight::view()->display('brachiosaurus.twig');
});

Flight::start();





